package com.example.testintent01;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        // Récupérer le texte envoyé depuis l'activité principale
        String userInput = getIntent().getStringExtra("user_input");

        // Afficher le texte dans le TextView
        TextView displayTextView = findViewById(R.id.display_text_view);
        displayTextView.setText(userInput);
    }
}
