package com.example.testintent01;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText editText;
    private Button button;
    private ActivityResultLauncher<Intent> activityResultLauncher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText = findViewById(R.id.edit_text);
        button = findViewById(R.id.button);

        activityResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == RESULT_OK) {
                        String userInput = result.getData().getStringExtra("user_input");

                        // Afficher le texte dans la zone de saisie
                        editText.setText(userInput);
                    }
                }
        );

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userInput = editText.getText().toString();

                // Créer une intention pour lancer la seconde activité
                Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                if (!TextUtils.isEmpty(userInput)) {
                    intent.putExtra("user_input", userInput);
                } else {
                    intent.putExtra("user_input", ""); // Envoyer une chaîne vide
                }

                // Lancer l'activité avec registerForActivityResult
                activityResultLauncher.launch(intent);
            }
        });
    }
}
